#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 08:27:02 2023

@author: Serrano Gutiérrez Luis Enrique
@author: Esquivel Guzman Karla Adriana
"""
# %% v1 09 02 2023
# datos de entrada 
d = 15.5
t = 3

# bloque que resuelve el problema
# haciendo uso de los datos de entrada
v = d/t

# Bloque que muestra el resultado
print(f"El resutlado es {v}")
# %% v2 09 02 2023
# datos de entrada 
dist = 15.5
tiempo = 3

# bloque que resuelve el problema
# haciendo uso de los datos de entrada
vel = dist/tiempo

# Bloque que muestra el resultado
print(f"El resutlado es {vel}")
# %%
resultado = v+d+t
print(f"La suma es {resultado}")
cadena1 = "una 'cadena"
cadena2 = 'otra "cadena'
cadena3 ="""una
cadena
multilinea"""
print(f"El valor es {cadena1} y la otra {cadena2} la trecera {cadena3}")
